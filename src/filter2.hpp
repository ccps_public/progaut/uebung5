#ifndef FILTER2_H
#define FILTER2_H

#include <cmath>

class Filter2 {
   public:
    Filter2(double a1, double a0, double b2, double b1, double b0){}

    double initialize(double u0) {return 0;}

    double step(double u) {
        return 0;
    }

    static Filter2 From_continuous_first_order(double a_0, double ts) {
        return Filter2{0,0,0,0,0};
    }

    static Filter2 From_continuous_second_order(double a_1, double a_0,
                                                double ts) {
        return Filter2{0,0,0,0,0};
    }

};

#endif  // FILTER_H
