#include "filter2.hpp"

#include <gtest/gtest.h>

TEST(filter2,static_gain) {
    Filter2 filter{0.0, 0.0, 10.0, 0.0, 0.0};

    const double us[]{1.0, 2.0, -1.0, 0.0, 5.0};

    for (const auto u : us) {
        const double y = filter.step(u);

        EXPECT_EQ(y, 10.0 * u);
    }
}

TEST(filter2,static_delay_zero_initialized) {
    Filter2 filter{0.0, 0.0, 0.0, 0.0, 10.0};

    const double us[]{1.0, 2.0, -1.0, 0.0, 5.0, 0.0, 0.0};
    const double us_delayed[]{0.0, 0.0, 1.0, 2.0, -1.0, 0.0, 5.0};

    for (size_t i = 0; i < 7; ++i) {
        const double y = filter.step(us[i]);

        EXPECT_EQ(y , 10.0 * us_delayed[i]);
    }
}

TEST(filter2,static_delay_initialized) {
    Filter2 filter{0.0, 0.0, 0.0, 0.0, 10.0};
    filter.initialize(10.0);

    const double us[]{1.0, 2.0, -1.0, 0.0, 5.0, 0.0, 0.0};
    const double us_delayed[]{10.0, 10.0, 1.0, 2.0, -1.0, 0.0, 5.0};

    for (size_t i = 0; i < 7; ++i) {
        const double y = filter.step(us[i]);

        EXPECT_EQ(y , 10.0 * us_delayed[i]);
    }
}

TEST(filter2,zero_initialized) {
    const double a1 = 4.0;
    const double a0 = 3.0;
    const double b2 = 2.0;
    const double b1 = 10.0;
    const double b0 = 5.0;

    Filter2 filter{a1, a0, b2, b1, b0};

    const double us[]{0.0, 0.0, 1.0, 2.0, -1.0, 0.0, 5.0};
    double ys[7]{0.0};

    for (size_t i = 2; i < 7; ++i) {
        const double y = filter.step(us[i]);

        ys[i] = -a1 * ys[i - 1] - a0 * ys[i - 2] + b2 * us[i] + b1 * us[i - 1] + b0 * us[i - 2];

        EXPECT_NEAR(y, ys[i], 1e-12);
    }
}

TEST(filter2,initialized) {
    const double a1 = 4.0;
    const double a0 = 3.0;
    const double b2 = 2.0;
    const double b1 = 10.0;
    const double b0 = 5.0;

    const double k = (b2 + b1 + b0) / (1.0 + a1 + a0);
    const double u0 = -7.0;
    const double y0c = u0 * k;

    Filter2 filter{a1, a0, b2, b1, b0};
    const double y0 = filter.initialize(u0);

    EXPECT_EQ(y0 , y0c);

    const double us[]{u0, u0, 1.0, 2.0, -1.0, 0.0, 5.0};
    double ys[7]{y0c, y0c, 0.0};

    for (size_t i = 2; i < 7; ++i) {
        const double y = filter.step(us[i]);

        ys[i] = -a1 * ys[i - 1] - a0 * ys[i - 2] + b2 * us[i] + b1 * us[i - 1] + b0 * us[i - 2];

        EXPECT_NEAR(y, ys[i], 1e-12);
    }
}

TEST(filter2,reinitialized) {
    const double a1 = 4.0;
    const double a0 = 3.0;
    const double b2 = 2.0;
    const double b1 = 10.0;
    const double b0 = 5.0;

    const double k = (b2 + b1 + b0) / (1.0 + a1 + a0);
    const double u0 = -7.0;
    const double y0c = u0 * k;

    Filter2 filter{a1, a0, b2, b1, b0};
    const double y0 = filter.initialize(u0);

    EXPECT_EQ(y0 , y0c);

    const double us[]{u0, u0, 1.0, 2.0, -1.0, 0.0, 5.0};
    double ys[7]{y0c, y0c, 0.0};

    for (size_t i = 2; i < 7; ++i) {
        const double y = filter.step(us[i]);

        ys[i] = -a1 * ys[i - 1] - a0 * ys[i - 2] + b2 * us[i] + b1 * us[i - 1] + b0 * us[i - 2];

        EXPECT_NEAR(y, ys[i], 1e-12);
    }

    const double y0r = filter.initialize(u0);
    EXPECT_EQ(y0r , y0c);

    for (size_t i = 2; i < 7; ++i) {
        const double y = filter.step(us[i]);

        ys[i] = -a1 * ys[i - 1] - a0 * ys[i - 2] + b2 * us[i] + b1 * us[i - 1] + b0 * us[i - 2];

        EXPECT_NEAR(y, ys[i], 1e-12);
    }
}

TEST(filter2,pt1) {
    Filter2 filter = Filter2::From_continuous_first_order(10.0, 0.01);

    const double u = 1.0;
    constexpr size_t N = 51;
    const double ys[N] = {
        4.7619047619047616e-02, 1.3832199546485260e-01, 2.2038656732534281e-01,
        2.9463546567531013e-01, 3.6181304037289957e-01, 4.2259275081357578e-01,
        4.7758391740275896e-01, 5.2733783003106760e-01, 5.7235327479001352e-01,
        6.1308153433382173e-01, 6.4993091201631481e-01, 6.8327082515761806e-01,
        7.1343550847594006e-01, 7.4072736481156476e-01, 7.6541999673427275e-01,
        7.8776094942624664e-01, 8.0797419233803258e-01, 8.2626236449631507e-01,
        8.4280880597285635e-01, 8.5777939588020324e-01, 8.7132421532018378e-01,
        8.8357905195635666e-01, 8.9466676129384637e-01, 9.0469849831347993e-01,
        9.1377483180743413e-01, 9.2198675258767837e-01, 9.2941658567456609e-01,
        9.3613881561032164e-01, 9.4222083317124328e-01, 9.4772361096445812e-01,
        9.5270231468212863e-01, 9.5720685614097345e-01, 9.6128239365135681e-01,
        9.6496978473217987e-01, 9.6830599571006737e-01, 9.7132447230910846e-01,
        9.7405547494633615e-01, 9.7652638209430398e-01, 9.7876196475198918e-01,
        9.8078463477560918e-01, 9.8261466955888443e-01, 9.8427041531518100e-01,
        9.8576847099944942e-01, 9.8712385471378750e-01, 9.8835015426485529e-01,
        9.8945966338248803e-01, 9.9046350496510815e-01, 9.9137174258747873e-01,
        9.9219348138867114e-01, 9.9293695935165471e-01, 9.9360962988959223e-01,
    };

    for (size_t i = 0; i < N; ++i) {
        const double y = filter.step(u);

        EXPECT_NEAR(y, ys[i], 1e-12);
    }
}

TEST(filter2,pt2) {
    Filter2 filter = Filter2::From_continuous_second_order(20.0, 100.0, 0.01);

    const double u = 1.0;
    constexpr size_t N = 51;
    const double ys[N] = {
        2.2675736961451248e-03, 1.0905949681459887e-02, 2.6948647939901586e-02,
        4.8906968755180150e-02, 7.5508614876030125e-02, 1.0566997494433562e-01,
        1.3847172343612915e-01, 1.7313735679620385e-01, 2.0901432780709311e-01,
        2.4555747797421931e-01, 2.8231450132668118e-01, 3.1891320297051790e-01,
        3.5505034238444760e-01, 3.9048187517104815e-01, 4.2501442808551215e-01,
        4.5849786094215494e-01, 4.9081878569834420e-01, 5.2189492786204250e-01,
        5.5167022856418979e-01, 5.8011059736060344e-01, 6.0720023624056474e-01,
        6.3293846456415614e-01, 6.5733698285567532e-01, 6.8041752066024630e-01,
        7.0220982012693378e-01, 7.2274991270508915e-01, 7.4207865141233120e-01,
        7.6024046562472358e-01, 7.7728230931672992e-01, 7.9325277719778986e-01,
        8.0820136630498152e-01, 8.2217786336275100e-01, 8.3523184065164857e-01,
        8.4741224527451819e-01, 8.5876706860276741e-01, 8.6934308435579910e-01,
        8.7918564523884057e-01, 8.8833852936088753e-01, 8.9684382879538971e-01,
        9.0474187364952519e-01, 9.1207118588930958e-01, 9.1886845794147498e-01,
        9.2516855177155621e-01, 9.3100451473203960e-01, 9.3640760899463926e-01,
        9.4140735183550095e-01, 9.4603156443914971e-01, 9.5030642723316483e-01,
        9.5425654006696725e-01, 9.5790498581013017e-01, 9.6127339617303575e-01,
    };

    for (size_t i = 0; i < N; ++i) {
        const double y = filter.step(u);

        EXPECT_NEAR(y, ys[i], 1e-12);
    }
}
