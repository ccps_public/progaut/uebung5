#ifndef FILTER_H
#define FILTER_H

#include <iostream>

class Filter {
   public:
    Filter(double a1, double a0, double b2, double b1, double b0){
    }

    ~Filter() {
    }

    Filter(const Filter& other){
    }

    Filter& operator=(const Filter& other) {
        return *this;
    }

    size_t order() const { return 0; }

    double initialize(double u0) {
        return 0;
    }

    double step(double u) {
        return 0;
    }

    Filter operator*(const Filter& other) const {
        Filter prod{0,0,0,0,0};

        return prod;
    }

    static Filter From_continuous_first_order(double a_0, double ts) {
        return Filter{0,0,0,0,0};
    }

    static Filter From_continuous_second_order(double a_1, double a_0,
                                               double ts) {

        return Filter{0,0,0,0,0};
    }

};

#endif  // FILTER_H
