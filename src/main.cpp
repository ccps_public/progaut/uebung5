#include <iostream>

#include "filter2.hpp"

int main() {
    Filter2 filter{0.0, 0.0, 0.5, 0.5, 0.0};

    const double u0 = 3.0;
    const double y0 = filter.initialize(u0);

    std::cout << "init: u0 = " << u0 << " -> y0 = " << y0 << "\n";

    const double us[] = {1.0, 2.0, 3.0, 0.5};

    for (const double u : us) {
        const double y = filter.step(u);

        std::cout << "u(k) = " << u << " -> y(k) = " << y << "\n";
    }

    return 0;
}
